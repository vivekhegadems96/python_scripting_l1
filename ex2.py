#Assignment 2
import sys

"""
isWhiteLine()->Function checks for spaces, tabs and newline using the isspace() function
and return True if spaces are present otherwise False
"""
def isWhiteLine(string):
		return string.isspace()

#Opening the external file, reading as the argument
with open(sys.argv[1],"r") as fd:
	for string in fd: 
		if (isWhiteLine(string) == False):
			print(str(string).strip())
fd.close()