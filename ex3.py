#Assignment 3


def isListOfInts(mylist):
	#print(mylist)
	if(type(mylist) is not list):
		raise ValueError(str(mylist) + " - arg not of <list> type")
	else:
		if len(mylist) == 0:
			return True
		else:
			flag = 1
			for element in mylist:
				if (type(element) is not int):
					flag = 0
			if (flag == 0):
				return False
			else:
				return True


def testList(argument):
	print(isListOfInts(argument))


testList([])
testList([1])
testList([1,2])
testList([0])
testList(['1'])
testList([1,'a'])
testList(['a',1])
testList([1,1.])
testList([1.,1.])
testList((1,2))
